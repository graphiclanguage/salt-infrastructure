/var/www/html:
  file.directory:
    - user: ubuntu
    - group: www-data
    - dir_mode: 774
    - file_mode: 664
    - recurse:
      - user
      - group
      - mode

/var/www/html/deploy-cache: file.directory
/var/www/html/releases: file.directory
/var/www/html/public: file.directory
/var/www/html/public/system: file.directory
/var/www/html/shared: file.directory
/var/www/html/shared/storage: file.directory
/var/www/html/shared/storage/framework: file.directory
/var/www/html/shared/storage/framework/cache: file.directory
/var/www/html/shared/storage/framework/sessions: file.directory
/var/www/html/shared/storage/framework/views: file.directory
