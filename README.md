# Salt Infrastructure

```
curl -L https://bootstrap.saltstack.com -o bootstrap_salt.sh
sudo sh bootstrap_salt.sh
```

Then create or symlink this repository to `/srv/salt`
```
sudo mkdir /srv/salt
sudo chown ubuntu /srv/salt
cd /srv/salt
git clone git@bitbucket.org:graphiclanguage/salt-infrastructure.git .
```

Then you can run salt to make the infrastructure changes
```
sudo salt-call --local state.apply --state-output=changes
```

