wkhtmltopdf:
  cmd.run:
    - unless: test -f /usr/local/bin/wkhtmltopdf
    - name: 'wget https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb && sudo apt-get -y install ./wkhtmltox_0.12.5-1.bionic_amd64.deb'
    