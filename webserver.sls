redis-server:
  pkg.installed: [] 
  service.running:
    - require: 
      - pkg: redis-server

supervisor:
  pkg.installed: [] 
  service.running:
    - require: 
      - pkg: supervisor

nginx:
  pkg.installed: []
  service.running:
    - require:
      - pkg: nginx

/etc/nginx/sites-available/default:
  file.managed:
    - require:
      - pkg: nginx
    - source:
      - '/srv/salt/files/nginx-default'
    - user: root