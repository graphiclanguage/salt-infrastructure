php-repo:
  pkgrepo.managed:
    - humanname: PHP Repo
    - ppa: ondrej/php

php7.2:
  pkg:
    - installed
    - require:
      - pkgrepo: php-repo
    - pkgs:
      - php7.2-cli
      - php7.2-common
      - php7.2-curl
      - php7.2-dev
      - php7.2-fpm
      - php7.2-gd
      - php7.2-intl
      - php7.2-mbstring 
      - php7.2-mysql
      - php7.2-xml
      - php7.2-xmlrpc
      - php7.2-zip

/etc/php/7.2/fpm/php.ini:
  file.managed:
    - require:
      - pkg: php7.2
    - source:
      - '/srv/salt/files/php.ini'
    - user: root

php7.2-fpm:
    service.running:
    - require:
      - pkg: php7.2